<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Remedios Model
 *
 * @property \App\Model\Table\ReceitasTable&\Cake\ORM\Association\HasMany $Receitas
 *
 * @method \App\Model\Entity\Remedio newEmptyEntity()
 * @method \App\Model\Entity\Remedio newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Remedio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Remedio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Remedio findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Remedio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Remedio[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Remedio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Remedio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Remedio[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Remedio[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Remedio[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Remedio[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RemediosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('remedios');
        $this->setDisplayField('id_remedio');
        $this->setPrimaryKey('id_remedio');

        $this->addBehavior('Timestamp');

        $this->hasMany('Receitas', [
            'foreignKey' => 'remedio_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('nome')
            ->requirePresence('nome', 'create')
            ->notEmptyString('nome');

        $validator
            ->scalar('bula')
            ->requirePresence('bula', 'create')
            ->notEmptyString('bula');

        $validator
            ->scalar('descricao')
            ->requirePresence('descricao', 'create')
            ->notEmptyString('descricao');

        $validator
            ->scalar('empresa_fabricante')
            ->requirePresence('empresa_fabricante', 'create')
            ->notEmptyString('empresa_fabricante');

        $validator
            ->scalar('marca')
            ->requirePresence('marca', 'create')
            ->notEmptyString('marca');

        return $validator;
    }
}
