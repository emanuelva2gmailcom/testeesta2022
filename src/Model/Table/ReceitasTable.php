<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Receitas Model
 *
 * @property \App\Model\Table\PacientesTable&\Cake\ORM\Association\BelongsTo $Pacientes
 * @property \App\Model\Table\RemediosTable&\Cake\ORM\Association\BelongsTo $Remedios
 *
 * @method \App\Model\Entity\Receita newEmptyEntity()
 * @method \App\Model\Entity\Receita newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Receita[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Receita get($primaryKey, $options = [])
 * @method \App\Model\Entity\Receita findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Receita patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Receita[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Receita|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Receita saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Receita[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Receita[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Receita[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Receita[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReceitasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('receitas');
        $this->setDisplayField('id_receita');
        $this->setPrimaryKey('id_receita');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pacientes', [
            'foreignKey' => 'paciente_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Remedios', [
            'foreignKey' => 'remedio_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('paciente_id')
            ->requirePresence('paciente_id', 'create')
            ->notEmptyString('paciente_id');

        $validator
            ->scalar('medico')
            ->requirePresence('medico', 'create')
            ->notEmptyString('medico');

        $validator
            ->scalar('descricao')
            ->requirePresence('descricao', 'create')
            ->notEmptyString('descricao');

        $validator
            ->integer('remedio_id')
            ->allowEmptyString('remedio_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('paciente_id', 'Pacientes'), ['errorField' => 'paciente_id']);
        $rules->add($rules->existsIn('remedio_id', 'Remedios'), ['errorField' => 'remedio_id']);

        return $rules;
    }
}
