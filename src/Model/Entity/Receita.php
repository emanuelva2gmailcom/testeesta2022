<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Receita Entity
 *
 * @property int $paciente_id
 * @property string $medico
 * @property string $descricao
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $id_receita
 * @property int|null $remedio_id
 *
 * @property \App\Model\Entity\Paciente $paciente
 * @property \App\Model\Entity\Remedio $remedio
 */
class Receita extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'paciente_id' => true,
        'medico' => true,
        'descricao' => true,
        'created' => true,
        'modified' => true,
        'remedio_id' => true,
        'paciente' => true,
        'remedio' => true,
    ];
}
