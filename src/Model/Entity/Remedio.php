<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Remedio Entity
 *
 * @property string $nome
 * @property string $bula
 * @property string $descricao
 * @property string $empresa_fabricante
 * @property string $marca
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $id_remedio
 *
 * @property \App\Model\Entity\Receita[] $receitas
 */
class Remedio extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'nome' => true,
        'bula' => true,
        'descricao' => true,
        'empresa_fabricante' => true,
        'marca' => true,
        'created' => true,
        'modified' => true,
        'receitas' => true,
    ];
}
