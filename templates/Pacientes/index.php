<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Paciente[]|\Cake\Collection\CollectionInterface $pacientes
 */
?>
<div class="pacientes index content">
    <?= $this->Html->link(__('New Paciente'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Pacientes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id_paciente') ?></th>
                    <th><?= $this->Paginator->sort('nome') ?></th>
                    <th><?= $this->Paginator->sort('pai') ?></th>
                    <th><?= $this->Paginator->sort('mae') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th><?= $this->Paginator->sort('cpf') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pacientes as $paciente): ?>
                <tr>
                    <td><?= $this->Number->format($paciente->id_paciente) ?></td>
                    <td><?= h($paciente->nome) ?></td>
                    <td><?= h($paciente->pai) ?></td>
                    <td><?= h($paciente->mae) ?></td>
                    <td><?= h($paciente->email) ?></td>
                    <td><?= h($paciente->cpf) ?></td>
                    <td><?= h($paciente->created) ?></td>
                    <td><?= h($paciente->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $paciente->id_paciente]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $paciente->id_paciente]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $paciente->id_paciente], ['confirm' => __('Are you sure you want to delete # {0}?', $paciente->id_paciente)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
