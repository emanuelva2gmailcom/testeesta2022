<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Paciente $paciente
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $paciente->id_paciente],
                ['confirm' => __('Are you sure you want to delete # {0}?', $paciente->id_paciente), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Pacientes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pacientes form content">
            <?= $this->Form->create($paciente) ?>
            <fieldset>
                <legend><?= __('Edit Paciente') ?></legend>
                <?php
                    echo $this->Form->control('nome');
                    echo $this->Form->control('pai');
                    echo $this->Form->control('mae');
                    echo $this->Form->control('email');
                    echo $this->Form->control('cpf');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
