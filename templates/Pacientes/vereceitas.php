<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Paciente $paciente
 */
?>
<?= $this->Html->css('teste') ?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Paciente'), ['action' => 'edit', $paciente->id_paciente], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Paciente'), ['action' => 'delete', $paciente->id_paciente], ['confirm' => __('Are you sure you want to delete # {0}?', $paciente->id_paciente), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Pacientes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Paciente'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
                <h4><?= __('Related Receitas') ?></h4>
                <?php if (!empty($paciente->receitas)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Paciente Id') ?></th>
                            <th><?= __('Medico') ?></th>
                            <th><?= __('Descricao') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Id Receita') ?></th>
                            <th><?= __('Remedio Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($paciente->receitas as $receitas) : debug([$receitas, $paciente->receitas]) ?>

                        <tr>
                            <td><?= h($receitas->paciente_id) ?></td>
                            <td><?= h($receitas->medico) ?></td>
                            <td><?= h($receitas->descricao) ?></td>
                            <td><?= h($receitas->created) ?></td>
                            <td><?= h($receitas->modified) ?></td>
                            <td><?= h($receitas->id_receita) ?></td>
                            <td><?= h($receitas->remedio_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Receitas', 'action' => 'view', $receitas->id_receita]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Receitas', 'action' => 'edit', $receitas->id_receita]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Receitas', 'action' => 'delete', $receitas->id_receita], ['confirm' => __('Are you sure you want to delete # {0}?', $receitas->id_receita)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
