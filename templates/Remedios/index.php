<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Remedio[]|\Cake\Collection\CollectionInterface $remedios
 */
?>
<div class="remedios index content">
    <?= $this->Html->link(__('New Remedio'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Remedios') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('nome') ?></th>
                    <th><?= $this->Paginator->sort('bula') ?></th>
                    <th><?= $this->Paginator->sort('descricao') ?></th>
                    <th><?= $this->Paginator->sort('empresa_fabricante') ?></th>
                    <th><?= $this->Paginator->sort('marca') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('id_remedio') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($remedios as $remedio): ?>
                <tr>
                    <td><?= h($remedio->nome) ?></td>
                    <td><?= h($remedio->bula) ?></td>
                    <td><?= h($remedio->descricao) ?></td>
                    <td><?= h($remedio->empresa_fabricante) ?></td>
                    <td><?= h($remedio->marca) ?></td>
                    <td><?= h($remedio->created) ?></td>
                    <td><?= h($remedio->modified) ?></td>
                    <td><?= $this->Number->format($remedio->id_remedio) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $remedio->id_remedio]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $remedio->id_remedio]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $remedio->id_remedio], ['confirm' => __('Are you sure you want to delete # {0}?', $remedio->id_remedio)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
