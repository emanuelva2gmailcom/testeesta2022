<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Remedio $remedio
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $remedio->id_remedio],
                ['confirm' => __('Are you sure you want to delete # {0}?', $remedio->id_remedio), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Remedios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="remedios form content">
            <?= $this->Form->create($remedio) ?>
            <fieldset>
                <legend><?= __('Edit Remedio') ?></legend>
                <?php
                    echo $this->Form->control('nome');
                    echo $this->Form->control('bula');
                    echo $this->Form->control('descricao');
                    echo $this->Form->control('empresa_fabricante');
                    echo $this->Form->control('marca');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
