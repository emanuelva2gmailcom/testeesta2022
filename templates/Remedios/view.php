<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Remedio $remedio
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Remedio'), ['action' => 'edit', $remedio->id_remedio], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Remedio'), ['action' => 'delete', $remedio->id_remedio], ['confirm' => __('Are you sure you want to delete # {0}?', $remedio->id_remedio), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Remedios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Remedio'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="remedios view content">
            <h3><?= h($remedio->id_remedio) ?></h3>
            <table>
                <tr>
                    <th><?= __('Nome') ?></th>
                    <td><?= h($remedio->nome) ?></td>
                </tr>
                <tr>
                    <th><?= __('Bula') ?></th>
                    <td><?= h($remedio->bula) ?></td>
                </tr>
                <tr>
                    <th><?= __('Descricao') ?></th>
                    <td><?= h($remedio->descricao) ?></td>
                </tr>
                <tr>
                    <th><?= __('Empresa Fabricante') ?></th>
                    <td><?= h($remedio->empresa_fabricante) ?></td>
                </tr>
                <tr>
                    <th><?= __('Marca') ?></th>
                    <td><?= h($remedio->marca) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Remedio') ?></th>
                    <td><?= $this->Number->format($remedio->id_remedio) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($remedio->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($remedio->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Receitas') ?></h4>
                <?php if (!empty($remedio->receitas)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Paciente Id') ?></th>
                            <th><?= __('Medico') ?></th>
                            <th><?= __('Descricao') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Id Receita') ?></th>
                            <th><?= __('Remedio Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($remedio->receitas as $receitas) : ?>
                        <tr>
                            <td><?= h($receitas->paciente_id) ?></td>
                            <td><?= h($receitas->medico) ?></td>
                            <td><?= h($receitas->descricao) ?></td>
                            <td><?= h($receitas->created) ?></td>
                            <td><?= h($receitas->modified) ?></td>
                            <td><?= h($receitas->id_receita) ?></td>
                            <td><?= h($receitas->remedio_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Receitas', 'action' => 'view', $receitas->id_receita]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Receitas', 'action' => 'edit', $receitas->id_receita]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Receitas', 'action' => 'delete', $receitas->id_receita], ['confirm' => __('Are you sure you want to delete # {0}?', $receitas->id_receita)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
