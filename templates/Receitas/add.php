<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receita $receita
 * @var \Cake\Collection\CollectionInterface|string[] $pacientes
 * @var \Cake\Collection\CollectionInterface|string[] $remedios
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Receitas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="receitas form content">
            <?= $this->Form->create($receita) ?>
            <fieldset>
                <legend><?= __('Add Receita') ?></legend>
                <?php
                    echo $this->Form->control('paciente_id', ['options' => $pacientes]);
                    echo $this->Form->control('medico');
                    echo $this->Form->control('descricao');
                    echo $this->Form->control('remedio_id', ['options' => $remedios, 'empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
