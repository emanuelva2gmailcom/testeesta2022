<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receita[]|\Cake\Collection\CollectionInterface $receitas
 */
?>
<div class="receitas index content">
    <?= $this->Html->link(__('New Receita'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Receitas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('paciente_id') ?></th>
                    <th><?= $this->Paginator->sort('medico') ?></th>
                    <th><?= $this->Paginator->sort('descricao') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('id_receita') ?></th>
                    <th><?= $this->Paginator->sort('remedio_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($receitas as $receita): ?>
                <tr>
                    <td><?= $receita->has('paciente') ? $this->Html->link($receita->paciente->id_paciente, ['controller' => 'Pacientes', 'action' => 'view', $receita->paciente->id_paciente]) : '' ?></td>
                    <td><?= h($receita->medico) ?></td>
                    <td><?= h($receita->descricao) ?></td>
                    <td><?= h($receita->created) ?></td>
                    <td><?= h($receita->modified) ?></td>
                    <td><?= $this->Number->format($receita->id_receita) ?></td>
                    <td><?= $receita->has('remedio') ? $this->Html->link($receita->remedio->id_remedio, ['controller' => 'Remedios', 'action' => 'view', $receita->remedio->id_remedio]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $receita->id_receita]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receita->id_receita]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receita->id_receita], ['confirm' => __('Are you sure you want to delete # {0}?', $receita->id_receita)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
