<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receita $receita
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Receita'), ['action' => 'edit', $receita->id_receita], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Receita'), ['action' => 'delete', $receita->id_receita], ['confirm' => __('Are you sure you want to delete # {0}?', $receita->id_receita), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Receitas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Receita'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="receitas view content">
            <h3><?= h($receita->id_receita) ?></h3>
            <table>
                <tr>
                    <th><?= __('Paciente') ?></th>
                    <td><?= $receita->has('paciente') ? $this->Html->link($receita->paciente->id_paciente, ['controller' => 'Pacientes', 'action' => 'view', $receita->paciente->id_paciente]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Medico') ?></th>
                    <td><?= h($receita->medico) ?></td>
                </tr>
                <tr>
                    <th><?= __('Descricao') ?></th>
                    <td><?= h($receita->descricao) ?></td>
                </tr>
                <tr>
                    <th><?= __('Remedio') ?></th>
                    <td><?= $receita->has('remedio') ? $this->Html->link($receita->remedio->id_remedio, ['controller' => 'Remedios', 'action' => 'view', $receita->remedio->id_remedio]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Receita') ?></th>
                    <td><?= $this->Number->format($receita->id_receita) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($receita->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($receita->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
