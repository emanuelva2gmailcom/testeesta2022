<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Receita $receita
 * @var string[]|\Cake\Collection\CollectionInterface $pacientes
 * @var string[]|\Cake\Collection\CollectionInterface $remedios
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receita->id_receita],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receita->id_receita), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Receitas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="receitas form content">
            <?= $this->Form->create($receita) ?>
            <fieldset>
                <legend><?= __('Edit Receita') ?></legend>
                <?php
                    echo $this->Form->control('paciente_id', ['options' => $pacientes]);
                    echo $this->Form->control('medico');
                    echo $this->Form->control('descricao');
                    echo $this->Form->control('remedio_id', ['options' => $remedios, 'empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
