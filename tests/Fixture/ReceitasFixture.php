<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReceitasFixture
 */
class ReceitasFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'paciente_id' => 1,
                'medico' => 'Lorem ipsum dolor sit amet',
                'descricao' => 'Lorem ipsum dolor sit amet',
                'created' => 1657023267,
                'modified' => 1657023267,
                'id_receita' => 1,
                'remedio_id' => 1,
            ],
        ];
        parent::init();
    }
}
