<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PacientesFixture
 */
class PacientesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_paciente' => 1,
                'nome' => 'Lorem ipsum dolor sit amet',
                'pai' => 'Lorem ipsum dolor sit amet',
                'mae' => 'Lorem ipsum dolor sit amet',
                'email' => 'Lorem ipsum dolor sit amet',
                'cpf' => 'Lorem ipsum dolor sit amet',
                'created' => 1657023015,
                'modified' => 1657023015,
            ],
        ];
        parent::init();
    }
}
