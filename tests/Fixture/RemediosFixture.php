<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RemediosFixture
 */
class RemediosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'nome' => 'Lorem ipsum dolor sit amet',
                'bula' => 'Lorem ipsum dolor sit amet',
                'descricao' => 'Lorem ipsum dolor sit amet',
                'empresa_fabricante' => 'Lorem ipsum dolor sit amet',
                'marca' => 'Lorem ipsum dolor sit amet',
                'created' => 1657023250,
                'modified' => 1657023250,
                'id_remedio' => 1,
            ],
        ];
        parent::init();
    }
}
